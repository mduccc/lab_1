import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lab_1/DBManager.dart';
import 'package:lab_1/InfoModel.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class ListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyListScreen();
  }
}

class _MyListScreen extends State<ListScreen> {
  final streamController = StreamController();
  @override
  void initState() {
    super.initState();

    databaseManager.open().then((_) async {
      streamController.add(await databaseManager.getAsList());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: streamController.stream,
        builder: (context, snapshot) {
          if (!snapshot.hasError) {
            if (snapshot.hasData) {
              List<InfoModel> list = snapshot.data;
              return Container(
                width: double.infinity,
                height: double.infinity,
                padding: EdgeInsets.all(15),
                child: ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Slidable(
                        secondaryActions: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 5),
                            child: IconSlideAction(
                              color: Colors.red,
                              icon: Icons.delete,
                              onTap: () {
                                databaseManager.delete(list[index].id);
                                list.removeAt(index);
                                streamController.add(list);
                              },
                            ),
                          )
                        ],
                        child: Container(
                          width: double.infinity,
                          color: Colors.green,
                          padding: EdgeInsets.all(5),
                          child: Column(
                            children: <Widget>[
                              Align(
                                alignment: FractionalOffset.centerLeft,
                                child: Container(
                                  margin: EdgeInsets.all(5),
                                  child: Text(
                                    'Username: ' + list[index].username,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                              Align(
                                alignment: FractionalOffset.centerLeft,
                                child: Container(
                                  margin: EdgeInsets.all(5),
                                  child: Text(
                                    'Password: ' + list[index].password,
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        actionPane: SlidableDrawerActionPane(),
                      ),
                    );
                  },
                ),
              );
            }
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        },
      ),
    );
  }
}
