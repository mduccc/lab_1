class InfoModel {
  int id;
  String username, password;

  InfoModel.fromMap(Map<String, dynamic> map) {
    this.id = map['id'] == null ? null : map['id'];
    this.username = map['username'];
    this.password = map['password'];
  }

  Map<String, dynamic> toMapWithoutId() {
    return {'username': this.username, 'password': this.password};
  }

  Map<String, dynamic> toMapWithId() {
    return {
      'id': this.id != null ? this.id : null,
      'username': this.username,
      'password': this.password
    };
  }
}
