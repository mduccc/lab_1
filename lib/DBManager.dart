import 'package:lab_1/InfoModel.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseManager {
  Database _db;
  Future open() async {
    this._db = await openDatabase('lab_1.db', version: 1,
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE _users (id integer primary key autoincrement, username text, password text)');
    }).whenComplete(() {
      print('db and table are CREATED');
    });
  }

  Future insert(InfoModel infoModel) async {
    try {
      print(infoModel.username);
      await this._db.insert('_users', infoModel.toMapWithoutId());
    } catch (e) {
      print(e);
    }
  }

  Future delete(int id) async {
    try {
      await this._db.delete('_users', where: 'id = ?', whereArgs: [id]);
    } catch (e) {
      print(e);
    }
  }

  Future<List<InfoModel>> getAsList() async {
    List<Map> list = await this._db.query('_users');
    return list.map((value) => InfoModel.fromMap(value)).toList();
  }
}

final databaseManager = DatabaseManager();
