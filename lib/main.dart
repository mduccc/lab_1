import 'package:flutter/material.dart';
import 'package:lab_1/DBManager.dart';
import 'package:lab_1/InfoModel.dart';
import 'package:lab_1/ListScreen.dart';
import 'package:toast/toast.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab_1',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController usernameEdittingController = TextEditingController();
  TextEditingController passwordEdittingController = TextEditingController();
  FocusNode usernameFocus = FocusNode();
  FocusNode passwordFocus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          padding: EdgeInsets.only(left: 25, right: 25),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/bg.png'), fit: BoxFit.cover),
          ),
          child: Align(
              alignment: FractionalOffset.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        child: Text(
                          'Register',
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.right,
                        ),
                      ),
                      Container(
                        height: 10,
                      ),
                      Container(
                        color: Colors.black.withOpacity(0.5),
                        child: TextField(
                          controller: usernameEdittingController,
                          focusNode: usernameFocus,
                          textInputAction: TextInputAction.next,
                          onSubmitted: (_) => {
                            FocusScope.of(context).requestFocus(passwordFocus)
                          },
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 15, right: 15, top: 15, bottom: 15),
                            hintText: 'Username',
                            hintStyle:
                                TextStyle(color: Colors.white.withOpacity(0.6)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black)),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.black.withOpacity(0.5))),
                          ),
                        ),
                      ),
                      Container(
                        height: 3,
                      ),
                      Container(
                        color: Colors.black.withOpacity(0.5),
                        child: TextField(
                          controller: passwordEdittingController,
                          focusNode: passwordFocus,
                          obscureText: true,
                          textInputAction: TextInputAction.done,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(
                                  left: 15, right: 15, top: 15, bottom: 15),
                              hintText: 'Password',
                              hintStyle: TextStyle(
                                  color: Colors.white.withOpacity(0.6)),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black)),
                              enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black.withOpacity(0.5)))),
                        ),
                      ),
                      Container(
                        width: double.infinity,
                        child: FlatButton(
                            color: Colors.pink,
                            child: Text('Login'),
                            onPressed: () async {
                              if (usernameEdittingController.text != null &&
                                  usernameEdittingController.text.isNotEmpty &&
                                  passwordEdittingController.text != null &&
                                  passwordEdittingController.text.isNotEmpty) {
                                await databaseManager.open();
                                await databaseManager
                                    .insert(InfoModel.fromMap({
                                  'id': null,
                                  'username': usernameEdittingController.text,
                                  'password': passwordEdittingController.text
                                }))
                                    .then((_) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ListScreen()));
                                });
                              } else {
                                Toast.show('Please fit all', context,
                                    duration: Toast.LENGTH_LONG,
                                    gravity: Toast.BOTTOM,
                                    backgroundColor: Colors.red);
                              }
                            }),
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Container(
                                height: 2,
                                color: Colors.white,
                              ),
                            ),
                            Expanded(
                              flex: 2,
                              child: Container(
                                child: Text(
                                  'Or login with',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                height: 2,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Container(
                                margin: EdgeInsets.all(5),
                                child: FlatButton(
                                  color: Colors.black.withOpacity(0.7),
                                  child: FittedBox(
                                    fit: BoxFit.fitWidth,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.face,
                                          color: Colors.white,
                                        ),
                                        Text(
                                          'Google',
                                          style: TextStyle(color: Colors.white),
                                          textAlign: TextAlign.center,
                                        )
                                      ],
                                    ),
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                margin: EdgeInsets.all(5),
                                child: FlatButton(
                                  color: Colors.black.withOpacity(0.7),
                                  child: FittedBox(
                                    fit: BoxFit.fitWidth,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.face,
                                          color: Colors.white,
                                        ),
                                        Text(
                                          'Facebook',
                                          style: TextStyle(color: Colors.white),
                                          textAlign: TextAlign.center,
                                        )
                                      ],
                                    ),
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                margin: EdgeInsets.all(5),
                                child: FlatButton(
                                  color: Colors.black.withOpacity(0.7),
                                  child: FittedBox(
                                    fit: BoxFit.fitWidth,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.face,
                                          color: Colors.white,
                                        ),
                                        Text(
                                          'Twitter',
                                          style: TextStyle(color: Colors.white),
                                        )
                                      ],
                                    ),
                                  ),
                                  onPressed: () {},
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ))),
    );
  }
}
